# spring-boot-react-starter #

Simple starter for Spring Boot and React apps

(Still being tested)

## To run ##

Run 

`./gradlew bootRun `

from the project root. App will start on localhost:8080, serving the React application from Spring Boot's static resources.

## To package ##

Run 

`./gradlew clean build`

from the project root. Outputs jar to `build/libs/`

## Configuration ##

Project uses the Copyfiles npm plugin to output the build files from `npm run build` to Spring Boot's static resources directory.

`npm run build` will output the built files to `src/main/resources/static`, which gradle's build then packages into the Spring Boot jar to be served as static content.

The output directory for `npm run build` is configured via `src/main/react-app/package.json` under scripts.build

## Notes ##

- Add the Spring Boot Dev Tools for backend hot reloading
- You can run the backend and frontend separately with

`./gradlew booRun`

from the project root to serve the backend (and currently packaged AngularJS app) on localhost:8080.

Go to 

`src/react-app` 

and run

`npm run start`

to run the frontend independently.

## Misc Dependencies ##
In order to use the commands specified in the auto-generated readme at `src/main/react-app` you will need `create-react-app` installed.

Install via npm with 

`npm install -g create-react-app`
