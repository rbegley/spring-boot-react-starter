package com.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactjsStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactjsStarterApplication.class, args);
	}
}
